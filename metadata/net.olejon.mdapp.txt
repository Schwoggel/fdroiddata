Categories:Office
License:GPLv3
Web Site:http://olejon.github.io/mdapp
Source Code:https://github.com/olejon/mdapp
Issue Tracker:https://github.com/olejon/mdapp/issues

Auto Name:LegeAppen
Summary:Toolkit for medical personnel
Description:
Useful and intuitive app for doctors and other health care workers in Norway,
optimized for both phones and tablets. Be aware that this app is in Norwegian
only.


Features:

* Medications, substances and favorites
* Diseases and treatments, with search in many recognized sources
* Emergency room handbook (this feature links to lvh.no from Gyldendal AS which is only available if you are located in Norway)
* Interaction analysis
* Poisonings information
* National guidelines
* ATC registry
* ICD-10
* Pharmaceutical companies
* Pharmacies
* Clinical trials
* Notifications from The Norwegian Medicines Agency
* Calculators
* Notes
* Tasks
* Various other features


Required Permissions:

* In-app purchases: To be able to donate money to the developer
* Location: To be able to find your position in relation to a pharmacy
* Photos/Media/Files: To be able to store files with content that is presented in the app
* Camera: To be able to scan barcodes
* Wi-Fi connection information: To be able to determine if the device is connected to Wi-Fi

Your position is not logged in any way.


Disclaimer:

LegeAppen is developed by a private person, and even though the content in the
app is fetched from recognized sources, the developer can not the be held
responsible for decisions taken based on information found in this app.


Credits:

* Stethoscrope icon from Icons8: http://icons8.com/web-app/957/Stethoscope
.

Repo Type:git
#Repo:https://github.com/olejon/mdapp
Repo:https://github.com/olejon/mdapp-fdroid

Build:1.1,110
    disable=builds, legal issues
    commit=45f68902deb7f8d830a930d3713c1866a52caa23
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Build:1.2,120
    disable=builds, legal issues
    commit=5090c3eb5ac0ee59bd24bd67a08768d30e194bb7
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Build:1.4,143
    commit=73793efc4d574e790d4802d28efd488c4c573ad5
    subdir=mdapp/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:143

