Categories:Reading
License:GPLv3
Web Site:
Source Code:https://github.com/dschuermann/document-viewer
Issue Tracker:https://github.com/dschuermann/document-viewer/issues

Auto Name:Document Viewer
Summary:Viewer for many document formats
Description:
Document Viewer is a fork of the last GPL version of
[https://code.google.com/p/ebookdroid EBookDroid]. Supports:

* PDF
* DjVu
* XPS (OpenXPS)
* Comic Books (cbz) (NO support for cbr (rar compressed))
* FictionBook (fb2, fb2.zip)

This apk supports all ABIs: ARM, x86 and MIPS.
.

Repo Type:git
Repo:https://github.com/dschuermann/document-viewer.git

Build:2.1,2100
    commit=v2.1
    subdir=document-viewer
    buildjni=yes

Build:2.2,2200
    commit=v2.2
    subdir=document-viewer
    buildjni=yes

Build:2.4,2400
    commit=6888f6bae47cf1a47d
    subdir=document-viewer
    buildjni=yes

Build:2.5,2500
    commit=v2.5
    subdir=document-viewer
    gradle=yes
    buildjni=yes

Build:2.6,2600
    commit=v2.6
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Build:2.6.1,2610
    commit=v2.6.1
    subdir=document-viewer
    submodules=yes
    gradle=yes
    prebuild=pushd ../ && \
        ./init.sh && \
        popd
    scandelete=document-viewer/jni/mupdf/mupdf/build/debug/
    buildjni=yes

Maintainer Notes:
Check init.sh from time to time.
.

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.6.1
Current Version Code:2610

