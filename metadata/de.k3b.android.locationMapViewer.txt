Categories:Navigation,System
License:GPLv3
Web Site:https://github.com/k3b/locationMapViewer/wiki
Source Code:https://github.com/k3b/locationMapViewer
Issue Tracker:https://github.com/k3b/locationMapViewer/issues

Name:Location Map Viewer
Auto Name:locationMapViewer
Summary:Display geografic information on a map
Description:
Display geografic information in a map. It has support for GPX and
KML files, as well as the "geo" URI scheme. It can work offline (without
Internet/WiFi) once geodata has been downloaded and cached.

Other Android apps can use LocationMapViewer through an [https://github.com/k3b/LocationMapViewer/blob/master/geoIntentDemo/src/main/java/de/k3b/android/locationMapViewer/demo/GeoIntentDemoActivity.java Intent interface]
or through html links like [geo:0,0?q=53.0,8.0(Hello) geo:0,0?q=53.0,8.0(Hello)]
(&lt;a href=&quot;geo:0,0?q=53.0,8.0(Hello)&quot;&gt;geo:0,0?q=53.0,8.0(Hello)&lt;/a&gt;)

Required Android Permissions:

* INTERNET: to download map data from Open Streetmap Server
* ACCESS_NETWORK_STATE and ACCESS_WIFI_STATE: to find out if wifi/internet is online to start downloaded geodata
* WRITE_EXTERNAL_STORAGE (to cache downloaded map data in local file system and to load gpx/kml-Files to be displayed in the map)
* ACCESS_FINE_LOCATION and ACCESS_COARSE_LOCATION: to display my own location in the map, too

[https://github.com/k3b/locationMapViewer/wiki/History Changelog]
.

Repo Type:git
Repo:https://github.com/k3b/locationMapViewer.git

Build:0.2.2.150321,2
    commit=Version0.2.2.150321
    subdir=LocationMapViewer
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.2.2.150321
Current Version Code:2

