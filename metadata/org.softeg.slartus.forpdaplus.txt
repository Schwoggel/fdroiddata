Categories:Internet
License:Apache2
Web Site:
Source Code:https://github.com/slartus/4pdaClient-plus
Issue Tracker:https://github.com/slartus/4pdaClient-plus/issues

Auto Name:4pda
Summary:Client for 4pda.ru
Description:
Client companion for the 4pda.ru forum.
.

Repo Type:git
Repo:https://github.com/slartus/4pdaClient-plus

Build:2.3beta3,458
    disable=this is not maintainable
    commit=2.3beta3
    srclibs=JSoup@jsoup-1.7.3,ApacheHttpClient@4.1.2,ImageLoader@v1.9.1
    rm=libs/jsoup-1.7.3.jar,libs/universal-image-loader-1.9.1.jar,libs/apache/httpclient-4.1.2.jar,libs/apache/httpmime-4.1.2.jar
    prebuild=pushd $$JSoup$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$JSoup$$/target/jsoup-1.7.3.jar libs/ && \
        pushd $$ImageLoader$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ImageLoader$$/target/universal-image-loader-1.9.1.jar libs/ && \
        pushd $$ApacheHttpClient$$/httpclient/ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$ApacheHttpClient$$/httpmime/ && \
        $$MVN3$$ package && \
        popd && \
        cp $$ApacheHttpClient$$/httpclient/target/httpclient-4.1.2.jar $$ApacheHttpClient$$/httpmime/target/httpmime-4.1.2.jar libs/apache/ && \
        rm build.xml && \
        android update project -p . -t "android-19" && \
        echo -e 'java.source=1.7\njava.target=1.7\n' | tee Externals/SqliteAnnotations/src/main/ant.properties > ant.properties && \
        echo -e "android.library.reference.1=Externals/4pdaApi" >> project.properties && \
        echo -e "android.library.reference.2=Externals/4pdaCommon" >> project.properties && \
        echo -e "android.library.reference.3=Externals/4pdaNotifyService" >> project.properties && \
        echo -e "android.library.reference.4=Externals/4pdaSources" >> project.properties && \
        echo -e "android.library.reference.5=Externals/ActionBar-PullToRefresh" >> project.properties && \
        echo -e "android.library.reference.6=Externals/NewQuickAction3D-master" >> project.properties && \
        echo -e "android.library.reference.7=Externals/QuickPost" >> project.properties && \
        echo -e "android.library.reference.8=Externals/SqliteAnnotations/src/main" >> project.properties && \
        echo -e "android.library.reference.9=Externals/android-sqlite-asset-helper-master/core" >> project.properties && \
        echo -e "android.library.reference.10=Externals/chrisbanes-Android-PullToRefresh" >> project.properties
    target=android-19

Build:2.3,459
    commit=dcd069fe1a0f2ad94d39c3360ccf88c00feefa06
    subdir=app
    gradle=yes
    forceversion=yes
    forcevercode=yes

Build:2.4b5,500
    commit=9f672b5bba8f244ee3b58041a59712622e0d1a41
    subdir=app
    gradle=yes

Build:2.4,501
    commit=0974170a4066fd599f17a97673c02fbf92cf6b2f
    subdir=app
    gradle=yes

Build:2.5.1,520
    commit=2.5.1
    subdir=app
    gradle=yes

Build:2.5.2,522
    commit=f1c2687b674581c45ba8231cb235f791b75bcba7
    subdir=app
    gradle=yes

Build:2.5.5,525
    commit=65f329b2689ccc625fabc6769dbb7ad1e2382c8f
    subdir=app
    gradle=yes

Auto Update Mode:None
#Auto Update Mode:Version %v
#Update Check Mode:Tags
Update Check Mode:RepoManifest
Current Version:2.5.6
Current Version Code:526

