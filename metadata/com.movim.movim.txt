Categories:Internet
License:AGPLv3+
Web Site:https://movim.eu
Source Code:https://github.com/edhelas/movim_android
Issue Tracker:https://bugs.launchpad.net/movim

Auto Name:Movim
Summary:Decentralized Social Network
Description:
Client for [https://movim.eu Movim], a distributed social networking
platform that tries to protect your privacy and comes with a set of
interesting features, like integrated Chats, Blogs, News and more --
all based on open and etablished protocols.
.

Repo Type:git
Repo:https://github.com/edhelas/movim_android.git

Build:0.9,1
    commit=595cb14
    rm=android/tests
    target=android-19

Build:0.9.0.1,2
    commit=d5631be
    rm=android/tests
    target=android-21

Build:0.9.0.2,3
    commit=34d6c1f
    rm=android/tests
    target=android-21

Maintainer Notes:
This is a first snapshot of the Android client of Movim
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.9.0.2
Current Version Code:3
