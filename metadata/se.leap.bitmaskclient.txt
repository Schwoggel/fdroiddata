Categories:System,Internet,Security
License:GPLv3
Web Site:https://leap.se
Source Code:https://github.com/leapcode/bitmask_android
Issue Tracker:https://leap.se/code/projects/android/issues

Auto Name:Bitmask
Summary:Easy and secure encrypted communication (VPN)
Description:
Bitmask is an application to provide easy and secure encrypted communication.
You can choose among several different service providers or start your own.
Currently, Bitmask supports encrypted internet (VPN) with encrypted email
coming soon.
.

Repo Type:git
Repo:https://github.com/leapcode/bitmask_android.git
Binaries:https://dl.bitmask.net/client/android/Bitmask-Android-%v.apk

Build:0.7.0,93
    disable=skip
    commit=0.7.0
    subdir=app
    gradle=yes
    build=misc/build-native.sh
    buildjni=no

Build:0.8.1,107
    disable=skip
    commit=0.8.1
    subdir=app
    gradle=yes
    build=misc/build-native.sh
    buildjni=no

Build:0.9.2RC2,114
    disable=does not verify
    commit=0.9.2RC2
    subdir=app
    gradle=yes
    build=misc/build-native.sh
    buildjni=no
    ndk=r10d

Build:0.9.2RC3,115
    disable=does not verify (all ndk-built files)
    commit=0.9.2RC3
    subdir=app
    gradle=yes
    build=misc/build-native.sh
    buildjni=no
    ndk=r10d

Build:0.9.2,116
    disable=does not verify (just MIPS)
    commit=0.9.2
    subdir=app
    gradle=yes
    build=misc/build-native.sh
    buildjni=no
    ndk=r10d

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.9.2
Current Version Code:116

