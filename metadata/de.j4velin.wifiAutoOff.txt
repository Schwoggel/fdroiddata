Categories:System
License:Apache2
Web Site:https://github.com/j4velin/WiFi-Auto-Off/blob/HEAD/README.md
Source Code:https://github.com/j4velin/WiFi-Auto-Off
Issue Tracker:https://github.com/j4velin/WiFi-Auto-Off/issues

Auto Name:WiFi Automatic
Summary:Turn off WiFi automatically
Description:
This simple Android app can help you increase the standby time of your device;
it automatically disables your WiFi radio when you don't need it and thereby
lowers the battery consumption. It is designed to be used with WiFi-only
tablets - these devices normally don't require a constant internet connection
if you're not using them and turning WiFi off can save a lot of battery power.

You can also specify to automatically turn on WiFi again, if you turn on your
device. This way, you are always connected to your WiFi network when using the
the tablet.

This app has a similiar effect like setting the "WiFi sleep policy" in Android
to "always", except that you can now exactly define the timeout between
turning the screen off and actually turning off WiFi.

If your device has a cell radio, it might switch to 2G/3G which may consume
more power than staying on WiFi.
.

Repo Type:git
Repo:https://github.com/j4velin/WiFi-Auto-Off.git

Build:1.3.1,8
    commit=b93697dd4fca0967e31ee84bc3a15573a09243a9
    target=android-18

Build:1.3.2,9
    commit=6b772091b7a97
    target=android-18

Build:1.3.3,10
    commit=ab5c819ec9a69
    target=android-18

Build:1.3.6,136
    commit=c2b574a9aad24ae80d37
    target=android-19

Build:1.3.7,137
    commit=0da9deda2
    target=android-19

Build:1.3.9,139
    commit=015e5671cbe
    target=android-19

Build:1.4,140
    commit=e00953f5800c86cfb0b9
    target=android-19

Build:1.4.2,142
    commit=415388357b4e669d8dd46cce332ebe6c65854f55
    gradle=yes
    prebuild=touch key.properties
    target=android-19

Build:1.4.4,144
    commit=bc58b160be5d1c5f075f801eaa499cfe00cb6cfd
    gradle=yes
    prebuild=touch key.properties
    target=android-19

Build:1.4.5,145
    disable=ZipException: invalid entry compressed size
    commit=b6a168fdc31278ec32fe248c653a12147c074ec6
    gradle=yes
    prebuild=touch key.properties && \
        sed -i -e '/shrinkResources/d' -e '/minifyEnabled/d' build.gradle

Build:1.4.6,146
    commit=78dfa585c76abb89c236bbaa26aa65a4c37ea303
    gradle=yes
    prebuild=touch key.properties

Build:1.4.7,147
    commit=f9e3567413d8c13314e0b7edb34812933d954572
    gradle=yes
    prebuild=touch key.properties

Build:1.4.8,148
    commit=ec79f619f1a014a4ef807b7333a6757f2065a33d
    gradle=yes
    prebuild=touch key.properties

Build:1.4.9,149
    commit=05315cf5ef20055c14f07c368d05823437746a91
    gradle=yes
    prebuild=touch key.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4.9
Current Version Code:149

